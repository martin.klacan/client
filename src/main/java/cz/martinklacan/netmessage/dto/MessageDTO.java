package cz.martinklacan.netmessage.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MessageDTO implements Serializable {

    private String user;
    private Date date;
    private List<ThreadInformationDTO> list;

    public MessageDTO(String user, Date date, List<ThreadInformationDTO> list) {
        this.user = user;
        this.date = date;
        this.list = list;
    }

    public String getUser() {
        return user;
    }

    public Date getDate() {
        return date;
    }

    public List<ThreadInformationDTO> getList() {
        return list;
    }
}