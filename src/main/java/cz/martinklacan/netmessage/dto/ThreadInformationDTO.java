package cz.martinklacan.netmessage.dto;

import java.io.Serializable;

public class ThreadInformationDTO implements Serializable {

    private final String name;
    private final String state;
    private final Long time;

    public ThreadInformationDTO(String name, String state, Long time) {
        this.name = name;
        this.state = state;
        this.time = time;
    }


}
