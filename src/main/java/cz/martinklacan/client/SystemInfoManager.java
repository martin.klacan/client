package cz.martinklacan.client;

import cz.martinklacan.netmessage.dto.ThreadInformationDTO;

import java.lang.management.*;
import java.util.LinkedList;
import java.util.List;


public class SystemInfoManager {

    private ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

    public List<ThreadInformationDTO> getCPUInfo () {

        List<ThreadInformationDTO> list = new LinkedList<>();
        for (Long threadID : threadMXBean.getAllThreadIds()) {
            ThreadInfo info = threadMXBean.getThreadInfo(threadID);
            list.add(new ThreadInformationDTO(info.getThreadName(), info.getThreadState().toString(), threadMXBean.getThreadCpuTime(threadID)));
        }
        return list;
    }
}
