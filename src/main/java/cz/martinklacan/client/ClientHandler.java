package cz.martinklacan.client;

import cz.martinklacan.netmessage.dto.MessageDTO;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ClientHandler extends ChannelInboundHandlerAdapter {

    private static final int DELAY = 10;
    private SystemInfoManager systemInfoManager;

    @Override
    public void channelActive(ChannelHandlerContext ctx){

        systemInfoManager = new SystemInfoManager();
        Runnable runnable = () -> ctx.writeAndFlush(new MessageDTO("client_01", new Date(), systemInfoManager.getCPUInfo()));
        ctx.channel().eventLoop().scheduleWithFixedDelay(runnable, DELAY, DELAY, TimeUnit.SECONDS);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable cause){
        cause.printStackTrace();
        channelHandlerContext.close();
    }
}